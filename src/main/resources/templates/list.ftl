<div class="generic-container">
 
      <div class="panel-heading" style="background-color: #2e88c7;font-weight: 500"><span class="lead">Java Assessment</span></div>
      <br>
    <div class="panel panel-default">
   

        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Country </span></div>
		<div class="panel-body">

	        <div class="formcontainer">
	            <div class="alert alert-success" role="alert" ng-if="ctrl.successMessage">{{ctrl.successMessage}}</div>
	            <div class="alert alert-danger" role="alert" ng-if="ctrl.errorMessage">{{ctrl.errorMessage}}</div>
	            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
	                <input type="hidden" ng-model="ctrl.country.country_id" />
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="uname">Name</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.country.name" id="uname" class="username form-control input-sm" placeholder="Enter country name" />
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="region">Region</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.country.region" id="region" class="form-control input-sm" placeholder="Enter country region." />
	                        </div>
	                    </div>
	                </div>
	
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="subregion">Sub Region</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.country.subregion" id="subregion" class="form-control input-sm" placeholder="Enter country sub region." />
	                        </div>
	                    </div>
	                </div>
	                
	                 <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="createdby">Create By</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.country.createdby" id="createdby" class="form-control input-sm" placeholder="Enter user name." />
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-actions floatRight">	                    
	                        <input type="submit"  value="{{!ctrl.country.country_id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid || myForm.$pristine">
	                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
	                        <button type="submit" ng-click="ctrl.loadCountryExt()" class="btn btn-warning btn-sm" >Get Latest Country Data</button>
	                        <button type="submit" ng-click="ctrl.loadDialog()"  class="btn btn-info btn-sm"  data-toggle="modal" data-target="#fileContentModal">View Save Encrypted Id</button>
	                    </div>
	                </div>
	            </form>
    	    </div>
		</div>	
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Country </span></div>
		<div class="panel-body">
			<div class="table-responsive">
		        <table class="table table-hover">
		            <thead>
		            <tr>
		                <th>ID</th>
		                <th>NAME</th>
		                <th>REGION</th>
		                <th>SUB REGION</th>
		                <th>FLAG</th>
		                <th width="100"></th>
		                <th width="100"></th>
		            </tr>
		            </thead>
		            <tbody>
		            <tr ng-repeat="u in ctrl.getAllCountrys()">
		                <td>{{u.country_id}}</td>
		                <td>{{u.name}}</td>		          
		                <td>{{u.region}}</td>
		                <td>{{u.subregion}}</td>
		                 <td><p><img src={{u.flag}} height='20px' width='20px'> </p></td>
		                <td><button type="button" ng-click="ctrl.editCountry(u.country_id)" class="btn btn-success custom-width">Edit</button></td>
		                <td><button type="button" ng-click="ctrl.removeCountry(u.country_id)" class="btn btn-danger custom-width">Remove</button></td>
		            </tr>
		            </tbody>
		        </table>		
			</div>
		</div>
    </div>
</div>

<div class="modal fade" id="fileContentModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">File Content : </h4>
        </div>
        <div class="modal-body">
					<div>{{ctrl.msg}}</div>
		            <div class="alert alert-success" role="alert" ng-if="ctrl.contents">
		            <ul><li ng-repeat="q in ctrl.contents">
					{{q.content}}
					</li></ul>
		            </div>                      
		        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>


