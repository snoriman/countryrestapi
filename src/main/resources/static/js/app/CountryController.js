'use strict';

angular.module('paynetApp').controller('CountryController',
    ['CountryService', '$scope',  function( CountryService, $scope) {

        var self = this;
        self.country = {};
        self.countrys=[];
        self.filedata={};

        self.submit = submit;
        self.getAllCountrys = getAllCountrys;
        self.createCountry = createCountry;
        self.updateCountry = updateCountry;
        self.removeCountry = removeCountry;
        self.editCountry = editCountry;
        self.loadCountryExt=loadCountryExt;
        self.loadDialog=loadDialog;
        self.reset = reset;

        self.content = '';
        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

        function submit() {
            console.log('Submitting');
            if (self.country.country_id === undefined || self.country.country_id === null) {
                console.log('Saving New Country', self.country);
                createCountry(self.country);
            } else {      
            		//do update
            		updateCountry(self.country, self.country.country_id);
                    console.log('Country updated with id ', self.country.country_id);
            	
                
            }
        }

        function createCountry(country) {
            console.log('About to create country');
            console.log("Created by ori",country.createdby);
            
            //this where the encryption is done on client
           var iv = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
           var salt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
           var aesUtil = new AesUtil(128, 1000);
           var ciphertext = aesUtil.encrypt(salt, iv, "1234567891234567", country.createdby);
           var payload = (iv + "#" + salt + "#" + ciphertext);
           var base64encodedPayload = btoa(payload);
            

            country.createdby=base64encodedPayload;
           // console.log("Created by encrypted",country.createdby);
            CountryService.createCountry(country)
                .then(
                    function (response) {
                        console.log('Country created successfully');
                        self.successMessage = 'Country created successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.country={};
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Error while creating Country');
                        self.errorMessage = 'Error while creating Country: ' + errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }


        function updateCountry(country, id){
            console.log('About to update country');
            CountryService.updateCountry(country, id)
                .then(
                    function (response){
                        console.log('Country updated successfully');
                        self.successMessage='Country updated successfully';
                        self.errorMessage='';
                        self.done = true;
                        $scope.myForm.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while updating Country');
                        self.errorMessage='Error while updating Country '+errResponse.data;
                        self.successMessage='';
                    }
                );
        }


        function removeCountry(id){
            console.log('About to remove Country with id '+id);
            CountryService.removeCountry(id)
                .then(
                    function(){
                        console.log('Country '+id + ' removed successfully');
                    },
                    function(errResponse){
                        console.error('Error while removing country '+id +', Error :'+errResponse.data);
                    }
                );
        }


        function getAllCountrys(){
            return CountryService.getAllCountrys();
        }

        function editCountry(id) {
            self.successMessage='';
            self.errorMessage='';
            CountryService.getCountry(id).then(
                function (country) {
                    self.country = country;
                },
                function (errResponse) {
                    console.error('Error while removing country ' + id + ', Error :' + errResponse.data);
                }
            );
        }
        
        
        
        function loadCountryExt(){
        	console.log("calling countroller - loadCountryExt");
             CountryService.loadCountryFromExt().then(
                function (country) {
                    self.country = country;
                    console.log('Country data successfully uploaded');
                    self.successMessage='Country successfully uploaded' ;
                    self.errorMessage='';
                    self.done = true;
                    $scope.myForm.$setPristine();
                },
                function (errResponse) {
                    console.error('Error while removing country ' + id + ', Error :' + errResponse.data);
                }
            );
        }
        
        function loadDialog(){
        	console.log("calling countroller - loadDialog");
            CountryService.loadModalDialog().then(
               function (response) {
                   console.log('reload');
                   console.log(response);
                   console.log(response.data.msg);
                   self.msg = response.data.msg;
                   self.contents= response.data.contents;
                   self.successMessage='Successfully view encrypted file';
                   self.errorMessage='';
                   self.country={};
                   $scope.myForm.$setPristine();

               },
               function (errResponse) {
                   console.error('Error while viewing file ' + id + ', Error :' + errResponse.data);
               }
           );
       }
        
        function reset(){
            self.successMessage='';
            self.errorMessage='';
            self.country={};
            $scope.myForm.$setPristine(); //reset Form
        }
    }


    ]);