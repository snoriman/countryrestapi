var app = angular.module('paynetApp',['ui.router','ngStorage']);

app.constant('urls', {
    BASE: 'http://localhost:8080/',
    USER_SERVICE_API : 'http://localhost:8080/countries/'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/list',
                controller:'CountryController',
                controllerAs:'ctrl',
                resolve: {
                    users: function ($q, CountryService) {
                        console.log('Load all countrys');
                        var deferred = $q.defer();
                        CountryService.loadAllCountrys().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                }
            });
        $urlRouterProvider.otherwise('/');
    }]);

