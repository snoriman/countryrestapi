'use strict';

angular.module('paynetApp').factory('CountryService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
                loadAllCountrys: loadAllCountrys,
                getAllCountrys: getAllCountrys,
                getCountry: getCountry,
                createCountry: createCountry,
                updateCountry: updateCountry,
                loadCountryFromExt: loadCountryFromExt,
                loadModalDialog: loadModalDialog,
                removeCountry: removeCountry
            };

            return factory;

            function loadAllCountrys() {
                console.log('Fetching all countrys');
                var deferred = $q.defer();
                $http.get(urls.USER_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all countrys');
                            $localStorage.countrys = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading countrys');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllCountrys(){
                return $localStorage.countrys;
            }

            function getCountry(id) {
                console.log('Fetching Country with id :'+id);
                var deferred = $q.defer();
                $http.get(urls.USER_SERVICE_API + id)
                    .then(
                        function (response) {
                            console.log('Fetched successfully Country with id :'+id);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while loading country with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createCountry(country) {
                console.log('Creating Country');
                var deferred = $q.defer();
                $http.post(urls.USER_SERVICE_API, country)
                    .then(
                        function (response) {
                            loadAllCountrys();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                           console.error('Error while creating Country : '+errResponse.data.errorMessage);
                           deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function updateCountry(country, id) {
                console.log('Updating Country with id '+id);
                var deferred = $q.defer();
                $http.put(urls.USER_SERVICE_API + id, country)
                    .then(
                        function (response) {
                            loadAllCountrys();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while updating Country with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
               function loadCountryFromExt() {
                console.log('Fetching all country from external API');
                var deferred = $q.defer();
                $http.get(urls.USER_SERVICE_API+"loadext/")
                    .then(
                        function (response) {
                            console.log('Fetched successfully all countrys');
                              loadAllCountrys();
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading countrys');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
             }
               
               function loadModalDialog() {

                   var deferred = $q.defer();
                   $http.get(urls.USER_SERVICE_API+"fileret/")
                       .then(
                           function (response) {
                               console.log('Fetched successfully ');
                                console.log(response.data);
                                console.log(response.data.contents);
                                loadAllCountrys();
                                deferred.resolve(response);
                           },
                           function (errResponse) {
                               console.error('Error request');
                               deferred.reject(errResponse);
                           }
                       );
                   return deferred.promise;
                }  
            

            function removeCountry(id) {
                console.log('Removing Country with id '+id);
                var deferred = $q.defer();
                $http.delete(urls.USER_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllCountrys();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while removing Country with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            


        }
    ]);