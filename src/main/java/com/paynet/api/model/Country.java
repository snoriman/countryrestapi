package com.paynet.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * db hibernate table mapping
 * @author norirman
 *
 */
@Entity
@Table(name="COUNTRIES")
public class Country {
	
	@Override
	public String toString() {
		return "Country [country_id=" + country_id + ", name=" + name + ", topleveldomain=" + topleveldomain
				+ ", alpha2code=" + alpha2code + ", alpha3code=" + alpha3code + ", capital=" + capital + ", region="
				+ region + ", subregion=" + subregion + ", population=" + population + ", demonym=" + demonym
				+ ", area=" + area + ", nativename=" + nativename + ", numericcode=" + numericcode + ", currencies="
				+ currencies + ", language=" + language + ", relevance=" + relevance + ", createdby=" + createdby
				+ ", flag=" + flag + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alpha2code == null) ? 0 : alpha2code.hashCode());
		result = prime * result + ((alpha3code == null) ? 0 : alpha3code.hashCode());
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((capital == null) ? 0 : capital.hashCode());
		result = prime * result + (int) (country_id ^ (country_id >>> 32));
		result = prime * result + ((createdby == null) ? 0 : createdby.hashCode());
		result = prime * result + ((currencies == null) ? 0 : currencies.hashCode());
		result = prime * result + ((demonym == null) ? 0 : demonym.hashCode());
		result = prime * result + ((flag == null) ? 0 : flag.hashCode());
		result = prime * result + ((language == null) ? 0 : language.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nativename == null) ? 0 : nativename.hashCode());
		result = prime * result + ((numericcode == null) ? 0 : numericcode.hashCode());
		result = prime * result + ((population == null) ? 0 : population.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		result = prime * result + ((relevance == null) ? 0 : relevance.hashCode());
		result = prime * result + ((subregion == null) ? 0 : subregion.hashCode());
		result = prime * result + ((topleveldomain == null) ? 0 : topleveldomain.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (alpha2code == null) {
			if (other.alpha2code != null)
				return false;
		} else if (!alpha2code.equals(other.alpha2code))
			return false;
		if (alpha3code == null) {
			if (other.alpha3code != null)
				return false;
		} else if (!alpha3code.equals(other.alpha3code))
			return false;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (capital == null) {
			if (other.capital != null)
				return false;
		} else if (!capital.equals(other.capital))
			return false;
		if (country_id != other.country_id)
			return false;
		if (createdby == null) {
			if (other.createdby != null)
				return false;
		} else if (!createdby.equals(other.createdby))
			return false;
		if (currencies == null) {
			if (other.currencies != null)
				return false;
		} else if (!currencies.equals(other.currencies))
			return false;
		if (demonym == null) {
			if (other.demonym != null)
				return false;
		} else if (!demonym.equals(other.demonym))
			return false;
		if (flag == null) {
			if (other.flag != null)
				return false;
		} else if (!flag.equals(other.flag))
			return false;
		if (language == null) {
			if (other.language != null)
				return false;
		} else if (!language.equals(other.language))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nativename == null) {
			if (other.nativename != null)
				return false;
		} else if (!nativename.equals(other.nativename))
			return false;
		if (numericcode == null) {
			if (other.numericcode != null)
				return false;
		} else if (!numericcode.equals(other.numericcode))
			return false;
		if (population == null) {
			if (other.population != null)
				return false;
		} else if (!population.equals(other.population))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		if (relevance == null) {
			if (other.relevance != null)
				return false;
		} else if (!relevance.equals(other.relevance))
			return false;
		if (subregion == null) {
			if (other.subregion != null)
				return false;
		} else if (!subregion.equals(other.subregion))
			return false;
		if (topleveldomain == null) {
			if (other.topleveldomain != null)
				return false;
		} else if (!topleveldomain.equals(other.topleveldomain))
			return false;
		return true;
	}

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long country_id;
	

	@Column(name="NAME", nullable=false)
	private String name; 
	

	@Column(name="TOPLEVELDOMAIN", nullable=false)
	private String topleveldomain; 
	
	
	@Column(name="ALPHA2CODE", nullable=false)
	private String alpha2code; 
	
	
	@Column(name="ALPHA3CODE", nullable=false)
	private String alpha3code; 
	
	
	@Column(name="CAPITAL", nullable=false)
	private String capital; 

	@Column(name="REGION", nullable=false)
	private String region; 
	
	
	@Column(name="SUBREGION", nullable=false)
	private String subregion;
	
	@Column(name="POPULATION", nullable=false)
	private String population;
	
	@Column(name="DEMONYM", nullable=false)
	private String demonym;
	

	@Column(name="AREA", nullable=false)
	private String area;
	

	@Column(name="NATIVENAME", nullable=false)
	private String nativename;
	

	@Column(name="NUMERICCODE", nullable=false)
	private String numericcode;

	@Column(name="CURRENCIES", nullable=false)
	private String currencies; 

	@Column(name="LANGUAGE", nullable=false)
	private String language; 
	

	@Column(name="RELEVANCE", nullable=false)
	private String relevance;
	
	@Column(name="CREATEDBY", nullable=false)
	private String createdby;
	
	@Column(name="FLAG", nullable=false)
	private String flag;
	
	

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public long getCountry_id() {
		return country_id;
	}

	public void setCountry_id(long country_id) {
		this.country_id = country_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTopleveldomain() {
		return topleveldomain;
	}

	public void setTopleveldomain(String topleveldomain) {
		this.topleveldomain = topleveldomain;
	}

	public String getAlpha2code() {
		return alpha2code;
	}

	public void setAlpha2code(String alpha2code) {
		this.alpha2code = alpha2code;
	}

	public String getAlpha3code() {
		return alpha3code;
	}

	public void setAlpha3code(String alpha3code) {
		this.alpha3code = alpha3code;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSubregion() {
		return subregion;
	}

	public void setSubregion(String subregion) {
		this.subregion = subregion;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getDemonym() {
		return demonym;
	}

	public void setDemonym(String demonym) {
		this.demonym = demonym;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getNativename() {
		return nativename;
	}

	public void setNativename(String nativename) {
		this.nativename = nativename;
	}

	public String getNumericcode() {
		return numericcode;
	}

	public void setNumericcode(String numericcode) {
		this.numericcode = numericcode;
	}

	public String getCurrencies() {
		return currencies;
	}

	public void setCurrencies(String currencies) {
		this.currencies = currencies;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getRelevance() {
		return relevance;
	}

	public void setRelevance(String relevance) {
		this.relevance = relevance;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	
	
	
	
	
}
