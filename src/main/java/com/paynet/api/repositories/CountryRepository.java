package com.paynet.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paynet.api.model.Country;

/**
 * 
 * @author norirman
 *
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
	Country findByName(String name);
}
