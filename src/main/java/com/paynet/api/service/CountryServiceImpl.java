package com.paynet.api.service;

import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paynet.api.common.CountryResponse;
import com.paynet.api.common.Language;
import com.paynet.api.model.Country;
import com.paynet.api.repositories.CountryRepository;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * 
 * @author norirman
 *
 */

@Service("countryService")
@Transactional
public class CountryServiceImpl implements CountryService {
	public static final Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);
	@Autowired
	private CountryRepository countryRepository;

	RestTemplate restTemplate = new RestTemplate();

	@Value("${api.url}")
	private String externalApiUri;

	@Override
	public Country findById(Long id) {
		return countryRepository.findOne(id);
	}

	@Override
	public Country findByName(String name) {
		return countryRepository.findByName(name);
	}

	@Override
	public void saveCountry(Country country) {
		countryRepository.save(country);

	}

	@Override
	public void updateCountry(Country country) {
		saveCountry(country);

	}

	@Override
	public void deleteCountryById(Long id) {
		countryRepository.delete(id);

	}

	@Override
	public void deleteAllCountry() {
		countryRepository.deleteAll();

	}

	@Override
	public List<Country> findAllCountry() {
		return countryRepository.findAll();
	}

	@Override
	public boolean isCountryExist(Country country) {
		return findByName(country.getName()) != null;
	}

	@Override
	public void loadDataFrExt() throws JsonParseException, JsonMappingException, IOException {
		// load up country from rapi api
		// https://restcountries.eu/rest/v2/regionalbloc/ASEAN
		// https://restcountries.eu/rest/v2/all"

		// disable ssl check
		try {

			acceptEveryCertificate();
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// RestTemplate restTemplate = new RestTemplate();
		logger.info("Fetching data from {}", externalApiUri);
		String ret = restTemplate.getForObject(externalApiUri, String.class);

		String jsonStr = ret;
		logger.info("JSON:{}", ret);
		ObjectMapper mapper = new ObjectMapper();
		CountryResponse[] jsonObj;

		jsonObj = mapper.readValue(jsonStr, CountryResponse[].class);
		Country cc = new Country();
		for (CountryResponse itr : jsonObj) {
			if (findByName(itr.getName()) != null) {
				// country already exist

			} else {
				cc = new Country();
				System.out.println("Val of name is: " + itr.getName());
				System.out.println("Val of name is: " + itr.getTopLevelDomain().get(0));
				cc.setName(itr.getName());
				cc.setRegion(itr.getRegion());
				cc.setSubregion(itr.getSubregion());
				cc.setAlpha2code(itr.getAlpha2Code());
				cc.setAlpha3code(itr.getAlpha3Code());
				cc.setCapital(itr.getCapital());
				cc.setDemonym(itr.getDemonym());
				cc.setArea("" + itr.getArea());
				cc.setTopleveldomain(itr.getTopLevelDomain().get(0).toString());
				cc.setNativename(itr.getNativeName());
				cc.setCurrencies(
						itr.getCurrencies().get(0).getName() + " (" + itr.getCurrencies().get(0).getCode() + ")");
				cc.setLanguage(itr.getLanguages().get(0).getName().toString());
				cc.setNumericcode(itr.getNumericCode());
				cc.setPopulation("" + itr.getPopulation());
				cc.setFlag(itr.getFlag());
				cc.setCreatedby("rest-api");
				countryRepository.save(cc);

			}

		}

	}

	protected void acceptEveryCertificate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

		TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
			@Override
			public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
				return true;
			}
		};

		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create()
				.setSSLContext(SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build()).build()));
	}

}
