package com.paynet.api.service;

import java.io.IOException;
import java.util.List;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.paynet.api.model.Country;

/**
 * 
 * @author norirman
 *
 */
public interface CountryService {
	Country findById(Long id);

	Country findByName(String name);

	void saveCountry(Country student);

	void updateCountry(Country student);

	void deleteCountryById(Long id);

	void deleteAllCountry();

	void loadDataFrExt() throws JsonParseException, JsonMappingException, IOException;

	List<Country> findAllCountry();

	boolean isCountryExist(Country student);

}
