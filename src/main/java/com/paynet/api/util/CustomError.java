package com.paynet.api.util;

/**
 * 
 * @author norirman
 *
 */
public class CustomError {
	private String errorMessage;

    public CustomError(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
