package com.paynet.api.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.paynet.api.common.ApiResponse;
import com.paynet.api.common.ApiResponseContent;
import com.paynet.api.common.Content;
import com.paynet.api.model.Country;
import com.paynet.api.service.CountryService;
import com.paynet.api.util.CustomError;
import com.paynet.api.util.EncrytionUtil;
import com.paynet.api.util.FileUtils;

import io.swagger.annotations.Api;

/**
 * 
 * rest api implementation
 * 
 * @author norirman
 *
 */
@RestController
@Api(value = "Country Resource REST Endpoint", description = "Shows the country info")
public class RestApiController {
	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	CountryService countryService;

	@Autowired
	FileUtils fileutil;

	@Value("${api.encryptfile}")
	private String uidFile;

	@Value("${api.checksumfile}")
	private String chksumFile;

	/*
	 * api controller implementation
	 */

	@RequestMapping(value = "/countries/", method = RequestMethod.GET)
	public ResponseEntity<List<Country>> listAllCountry() {
		List<Country> countrys = countryService.findAllCountry();
		if (countrys.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Country>>(countrys, HttpStatus.OK);
	}

	@RequestMapping(value = "/countries/", method = RequestMethod.POST)
	public ResponseEntity<?> createCountry(@RequestBody Country country, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Country : {}", country);
		EncrytionUtil aesUtil = new EncrytionUtil(128, 1000);

		if (countryService.isCountryExist(country)) {
			logger.error("Unable to create. A Country with name {} already exist", country.getName());
			return new ResponseEntity(
					new CustomError("Unable to create. A Country with name " + country.getName() + " already exist."),
					HttpStatus.CONFLICT);
		}

	

		if(country.getName()==null || country.getName().equalsIgnoreCase("") ) {
			//dun insert null country name, skip create country
			
		}else {
			// decoded and decrypt user id
			String payload = new String(java.util.Base64.getDecoder().decode(country.getCreatedby()));
			String clearTextUseriId = aesUtil.decrypt(payload.split("#")[1], payload.split("#")[0], "1234567891234567",
					payload.split("#")[2]);
			logger.info("Userid decrpt :{}", clearTextUseriId);
			country.setCreatedby(clearTextUseriId);
			// additional requirment to save the encrypt userid in file
			fileutil.saveEncryptUidToFile(payload + "\n");

			String checksumFileValue = "";
			try {
				checksumFileValue = fileutil.checksum();
				logger.info("Check sum value [{}]", checksumFileValue);
				// store checksum value for the file
				fileutil.saveCheckSumToFile(checksumFileValue + "\n");
			} catch (NoSuchAlgorithmException | IOException e) {
				e.printStackTrace();
			}
			countryService.saveCountry(country);
		}
	

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/countries/{id}").buildAndExpand(country.getCountry_id()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/countries/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getCountry(@PathVariable("id") long id) {
		logger.info("Fetching Country with id {}", id);
		Country country = countryService.findById(id);
		if (country == null) {
			logger.error("Country with id {} not found.", id);
			return new ResponseEntity(new CustomError("Country with id " + id + " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Country>(country, HttpStatus.OK);
	}

	@RequestMapping(value = "/countries/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateCountry(@PathVariable("id") long id, @RequestBody Country country) {
		logger.info("Updating Country with id {}", id);

		Country currentCountry = countryService.findById(id);

		if (currentCountry == null) {
			logger.error("Unable to update. Country with id {} not found.", id);
			return new ResponseEntity(new CustomError("Unable to upate. Country with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		currentCountry.setName(country.getName());
		currentCountry.setRegion(country.getRegion());
		currentCountry.setSubregion(country.getSubregion());

		countryService.updateCountry(currentCountry);
		return new ResponseEntity<Country>(currentCountry, HttpStatus.OK);
	}

	@RequestMapping(value = "/countries/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCountry(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Country with id {}", id);

		Country country = countryService.findById(id);
		if (country == null) {
			logger.error("Unable to delete. Country with id {} not found.", id);
			return new ResponseEntity(new CustomError("Unable to delete. Country with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		countryService.deleteCountryById(id);
		return new ResponseEntity<Country>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/countries/", method = RequestMethod.DELETE)
	public ResponseEntity<ApiResponse> deleteAllCountrys() {
		logger.info("Deleting All Countrys");

		countryService.deleteAllCountry();
		ApiResponse response = new ApiResponse();
		response.setStatus("Success");
		response.setMsg("Deleted all country data");
		return new ResponseEntity<ApiResponse>(HttpStatus.NO_CONTENT);
	}

	// load data for external rest api to db
	@RequestMapping(value = "/countries/loadext/", method = RequestMethod.GET)
	public ApiResponse loadCountryDataFromExternalRestApi() {
		logger.info("Load Db with Data");
		ApiResponse response = new ApiResponse();
		try {
			countryService.loadDataFrExt();
			response.setStatus("Success");
			response.setMsg("Data updated from source https://restcountries.eu/rest/v2");
		} catch (IOException e) {
			e.printStackTrace();
			response.setStatus("Error");
			response.setMsg(e.toString());
		}

		return response;
	}

	// to retrieve encrypt uid file
	@RequestMapping(value = "/countries/fileret/", method = RequestMethod.GET)
	public ApiResponseContent retrieveEncryptUidFileContent() {
		logger.info("retriving file content at {}", uidFile);
		ApiResponseContent response = new ApiResponseContent();
		EncrytionUtil aesUtil = new EncrytionUtil(128, 1000);

		// read the stored checksum
		StringBuilder cksumvalue = new StringBuilder();

		try (Stream<String> stream = Files.lines(Paths.get(chksumFile), StandardCharsets.UTF_8)) {
			stream.forEach(s -> cksumvalue.append(s).append("\n"));

			String ckval = cksumvalue.toString();

			// get current cksumvalue of file
			String currentFileCkVal = "";

			currentFileCkVal = fileutil.checksum();
			logger.info("Checking file intergrity , file :{}", uidFile);
			logger.info("Current MD5 cksum -{} , Stored value MD5 cksum-{}", currentFileCkVal, ckval);

			if (currentFileCkVal.equals(ckval.trim())) {
				// check sum check and valid
				logger.info("File ({}) integrity check valid ", uidFile);

				List<String> list = new ArrayList<>();

				try (BufferedReader br = Files.newBufferedReader(Paths.get(uidFile))) {

					list = br.lines().collect(Collectors.toList());

				} catch (IOException e) {
					e.printStackTrace();
				}

				// list.forEach(logger::info);

				List<Content> contents = new ArrayList<>();
				Content content = new Content();
				for (String payload : list) {
					content = new Content();
					String clearTextUseriId = aesUtil.decrypt(payload.split("#")[1], payload.split("#")[0],
							"1234567891234567", payload.split("#")[2]);
					logger.info("Userid decrpt :{}", clearTextUseriId);
					content.setContent(clearTextUseriId);
					contents.add(content);

				}
				response.setStatus("Success");
				response.setMsg("List of decrypted contents from "+uidFile+" , file checksum ["+currentFileCkVal+"].");
				response.setContents(contents);

			} else {
				// check sum invalid
				logger.info("File ({}) integrity check invalid ", uidFile);
				response.setStatus("Error");
				response.setMsg("Opss.. the file seem to be tempared , no view for now ...");
				

			}

		} catch (NoSuchAlgorithmException | IOException e) {
			logger.info("Error :{}",e.toString());
			response.setStatus("Success");
			response.setMsg("File content is empty!!!");
			//e.printStackTrace();
		}

		return response;
	}

}
