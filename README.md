# README #
Paynet Java Assessment -  Assigment 2

### Requirement ###
 
 * identify any publicly available service exposing REST API that returns data in JSON format
 * Issue one or more suitable GET requests to the relevant API endpoints to retrieve a small subset of the full dataset retrievable from that service.
 * Create a schema for a database table to hold this small data subset.
 * Design a REST API that is exposed by the app to that is capable of processing HTTP POST and GET methods. The GET simply returns the entire contents of the database table,while the POST provides information for appending a new record into the table.
 * Create a simple application client that can issue these GET and POST requests to test your REST API. Data returned from the GET request should be viewable by the user. Data for the POST requests can be obtained interactively from the user via the command line, from a HTML/JSP page or from a hardcoded JSON/text file.

### Situation ###

* Load country data from external api (https://restcountries.eu/rest/v2)
* create api to access the data save in database , CRUD functionality


### Technology Stack Used ###

* Springboot , hibernate,JPA (backend)
* Front-End (AngularJs,freemaker)
* Swagger
* db mysql


### How to run###

* create app db as per script provided (_appdb.sql)
* mvn clean install -DskipTest //to build
* mvn spring-boot:run  //run
* swagger -> http://localhost:8080/swagger-ui.html
* demo page -> http://localhost:8080/

