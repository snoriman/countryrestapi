-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 03, 2020 at 07:05 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `paynet_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `COUNTRIES`
--

CREATE TABLE `COUNTRIES` (
  `country_id` bigint(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `topleveldomain` varchar(256) DEFAULT NULL,
  `alpha2code` varchar(256) DEFAULT NULL,
  `alpha3code` varchar(256) DEFAULT NULL,
  `capital` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `region` varchar(256) DEFAULT NULL,
  `subregion` varchar(256) DEFAULT NULL,
  `population` varchar(256) DEFAULT NULL,
  `demonym` varchar(256) DEFAULT NULL,
  `area` varchar(256) DEFAULT NULL,
  `nativename` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `numericcode` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `currencies` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `language` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `relevance` varchar(256) DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Indexes for dumped tables
--

--
-- Indexes for table `COUNTRIES`
--
ALTER TABLE `COUNTRIES`
  ADD PRIMARY KEY (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `COUNTRIES`
--
ALTER TABLE `COUNTRIES`
  MODIFY `country_id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;
